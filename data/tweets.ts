import { profImage, contentImage } from './url';

const tweets = [
    {
        id: '1',
        user: {
            id: 'u1',
            username: 'jlspnd',
            name: 'Julius Pineda',
            image: profImage
        },
        createdAt: '2020-12-20T12:00:00.000Z',
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        image: contentImage,
        numberOfComments: 5,
        numberOfRetweets: 100,
        numberOfLikes: 10
    },
    {
        id: '2',
        user: {
            id: 'u1',
            username: 'jlspnd',
            name: 'Julius Pineda',
            image: profImage
        },
        createdAt: '2020-12-20T12:00:00.000Z',
        content: 'Bebe ko pandak!',
        numberOfComments: 123,
        numberOfRetweets: 11,
        numberOfLikes: 10
    },
    {
        id: '3',
        user: {
            id: 'u1',
            username: 'jlspnd',
            name: 'Julius Pineda',
            image: profImage
        },
        createdAt: '2020-12-20T12:00:00.000Z',
        content: "Hey you, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        numberOfComments: 123,
        numberOfRetweets: 11,
        numberOfLikes: 10
    }
];

export default tweets;