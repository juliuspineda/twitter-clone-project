import { StatusBar } from 'expo-status-bar';
import React, { useEffect } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Amplify, {
  API, 
  Auth,
  graphqlOperation
} from 'aws-amplify';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import config from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native';
import { getUser } from './graphql/queries';
import { createUser } from './graphql/mutations';
import {profImage} from './data/url';
import { CreateUserInput } from './API';

Amplify.configure(config)

function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  const getRandomImage = () => {
    return profImage;
  }

  const saveUserToDB = async (user: CreateUserInput) => {
    console.log('user - ', user);
    await API.graphql(graphqlOperation(createUser, { input: user }))
  }

  useEffect(() => {
    const updateUser = async () => {
      // Get current authenticated user
      const userInfo = await Auth.currentAuthenticatedUser({ bypassCache: true });
      if(userInfo) {
        // Check if user already exists in db
        const userData = await API.graphql(graphqlOperation(getUser, { id: userInfo.attributes.sub }));
        if(!userData.data.getUser) {
          const user = {
            id: userInfo.attributes.sub,
            username: userInfo.username,
            name: userInfo.username,
            email: userInfo.attributes.email,
            image: getRandomImage()
          }
          await saveUserToDB(user);
        } else {
          console.log('User already exists!');
        }
      }
      
      // if it doesn't, create the user in db
    }
    updateUser();
  }, [])

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}

export default withAuthenticator(App);